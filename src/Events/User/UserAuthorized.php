<?php

namespace Dogovor24\Queue\Events\User;

use Dogovor24\Queue\QueuePackageDispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class UserAuthorized
{
    use QueuePackageDispatchable, InteractsWithSockets, SerializesModels;

    public $user_id;
    public $uuid;

    public function __construct($user_id, $uuid)
    {
        $this->user_id = $user_id;
        $this->uuid = $uuid;
    }
}
