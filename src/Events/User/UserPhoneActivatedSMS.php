<?php

namespace Dogovor24\Queue\Events\User;

use Dogovor24\Queue\QueuePackageDispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class UserPhoneActivatedSMS
{
    use QueuePackageDispatchable, InteractsWithSockets, SerializesModels;

    public $user_id;
    public $phone;
    public $code;

    public function __construct($user_id, $phone, $code)
    {
        $this->user_id = $user_id;
        $this->email = $phone;
        $this->code = $code;
    }
}