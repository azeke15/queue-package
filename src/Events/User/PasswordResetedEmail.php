<?php

namespace Dogovor24\Queue\Events\User;

use Dogovor24\Queue\QueuePackageDispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class PasswordResetedEmail
{
    use QueuePackageDispatchable, InteractsWithSockets, SerializesModels;

    public $user_id;
    public $hash;

    public function __construct($user_id, $hash)
    {
        $this->user_id = $user_id;
        $this->hash = $hash;
    }
}
