<?php

namespace Dogovor24\Queue\Events\User;

use Dogovor24\Queue\QueuePackageDispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class UserRegisteredEmail
{
    use QueuePackageDispatchable, InteractsWithSockets, SerializesModels;

    public $user_id;
    public $password;

    public function __construct($user_id, $password)
    {
        $this->user_id = $user_id;
        $this->password = $password;
    }
}
