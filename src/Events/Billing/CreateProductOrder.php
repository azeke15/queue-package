<?php

namespace Dogovor24\Queue\Events\Billing;

use Dogovor24\Queue\QueuePackageDispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class CreateProductOrder
{
    use QueuePackageDispatchable, InteractsWithSockets, SerializesModels;

    public $user_id;
    public $product_id;

    public function __construct($user_id, $product_id)
    {
        $this->user_id = $user_id;
        $this->product_id = $product_id;
    }
}
