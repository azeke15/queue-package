<?php

namespace Dogovor24\Queue\Events\Billing;

use Dogovor24\Queue\QueuePackageDispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class PaymentSuccessedEmail
{
    use QueuePackageDispatchable, InteractsWithSockets, SerializesModels;

    public $order_id;
    public $user_id;
    public $products = [];
    public $payment;

    public function __construct($order_id, $user_id, array $products, $payment)
    {
        $this->order_id = $order_id;
        $this->user_id = $user_id;
        $this->products = $products;
        $this->payment = $payment;
    }
}
