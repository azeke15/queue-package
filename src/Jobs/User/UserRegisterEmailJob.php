<?php

namespace Dogovor24\Queue\Jobs\User;

use App\Events\TestEvent;
use App\Providers\EventServiceProvider;
use Dogovor24\Queue\Events\User\UserRegisteredEmail;
use Dogovor24\Queue\QueuePackageJob;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UserRegisterEmailJob extends QueuePackageJob
{
    protected $user_id;
    protected $password;

    public function __construct($user_id, $password)
    {
        $this->user_id = $user_id;
        $this->password = $password;
    }

    function eventProcessing()
    {
        $class = $this->eventClassName();
        event(new $class($this->user_id, $this->password));
    }

    function eventClassName()
    {
        return UserRegisteredEmail::class;
    }
}
