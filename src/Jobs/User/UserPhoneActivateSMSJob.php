<?php

namespace Dogovor24\Queue\Jobs\User;

use App\Events\TestEvent;
use App\Providers\EventServiceProvider;
use Dogovor24\Queue\Events\User\UserPhoneActivatedSMS;
use Dogovor24\Queue\QueuePackageJob;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UserPhoneActivateSMSJob extends QueuePackageJob
{
    protected $user_id;
    protected $phone;
    protected $code;

    public function __construct($user_id, $phone, $code)
    {
        $this->user_id = $user_id;
        $this->phone = $phone;
        $this->code = $code;
    }

    function eventProcessing()
    {
        $class = $this->eventClassName();
        event(new $class($this->user_id, $this->phone, $this->code));
    }

    function eventClassName()
    {
        return UserPhoneActivatedSMS::class;
    }
}