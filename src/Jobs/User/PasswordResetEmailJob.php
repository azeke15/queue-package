<?php

namespace Dogovor24\Queue\Jobs\User;

use App\Events\TestEvent;
use App\Providers\EventServiceProvider;
use Dogovor24\Queue\Events\User\PasswordResetedEmail;
use Dogovor24\Queue\QueuePackageJob;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PasswordResetEmailJob extends QueuePackageJob
{
    protected $user_id;
    protected $hash;

    public function __construct($user_id, $hash)
    {
        $this->user_id = $user_id;
        $this->hash = $hash;
    }


    function eventProcessing()
    {
        $class = $this->eventClassName();
        event(new $class($this->user_id, $this->hash));
    }

    function eventClassName()
    {
        return PasswordResetedEmail::class;
    }
}
