<?php

namespace Dogovor24\Queue\Jobs\User;

use App\Events\TestEvent;
use App\Providers\EventServiceProvider;
use Dogovor24\Queue\Events\User\UserAuthorized;
use Dogovor24\Queue\QueuePackageJob;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UserAuthorizedJob extends QueuePackageJob
{
    protected $user_id;
    protected $uuid;

    public function __construct($user_id, $uuid)
    {
        $this->user_id = $user_id;
        $this->uuid = $uuid;
    }


    function eventProcessing()
    {
        $class = $this->eventClassName();
        event(new $class($this->user_id, $this->uuid));
    }

    function eventClassName()
    {
        return UserAuthorized::class;
    }
}
