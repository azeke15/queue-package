<?php

namespace Dogovor24\Queue\Jobs\User;

use App\Events\TestEvent;
use App\Providers\EventServiceProvider;
use Dogovor24\Queue\Events\User\PasswordResetedSMS;
use Dogovor24\Queue\QueuePackageJob;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PasswordResetSMSJob extends QueuePackageJob
{
    protected $user_id;
    protected $code;

    public function __construct($user_id, $code)
    {
        $this->user_id = $user_id;
        $this->code = $code;
    }


    function eventProcessing()
    {
        $class = $this->eventClassName();
        event(new $class($this->user_id, $this->code));
    }

    function eventClassName()
    {
        return PasswordResetedSMS::class;
    }
}
