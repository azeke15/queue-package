<?php

namespace Dogovor24\Queue\Jobs\Billing;

use App\Events\TestEvent;
use App\Providers\EventServiceProvider;
use Dogovor24\Queue\Events\Billing\CreateProductOrder;
use Dogovor24\Queue\QueuePackageDispatchable;
use Dogovor24\Queue\QueuePackageJob;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateProductOrderJob extends QueuePackageJob
{
    protected $user_id;
    protected $product_id;

    public function __construct($user_id, $product_id)
    {
        $this->user_id = $user_id;
        $this->product_id = $product_id;
    }

    function eventProcessing()
    {
        $class = $this->eventClassName();
        event(new $class($this->user_id, $this->product_id));
    }

    function eventClassName()
    {
        return CreateProductOrder::class;
    }
}
