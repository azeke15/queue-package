<?php

namespace Dogovor24\Queue\Jobs\Billing;

use App\Events\TestEvent;
use App\Providers\EventServiceProvider;
use Dogovor24\Queue\Events\Billing\PaymentSuccessedEmail;
use Dogovor24\Queue\QueuePackageDispatchable;
use Dogovor24\Queue\QueuePackageJob;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PaymentSuccessEmailJob extends QueuePackageJob
{
    protected $order_id;
    protected $user_id;
    protected $products = [];
    protected $payment;

    public function __construct($order_id, $user_id, array $products, $payment)
    {
        $this->order_id = $order_id;
        $this->user_id = $user_id;
        $this->products = $products;
        $this->payment = $payment;
    }


    function eventProcessing()
    {
        $class = $this->eventClassName();
        event(new $class($this->order_id, $this->user_id, $this->products, $this->payment));
    }

    function eventClassName()
    {
        return PaymentSuccessedEmail::class;
    }
}
