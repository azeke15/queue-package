<?php
/**
 * Created by PhpStorm.
 * User: azamat
 * Date: 28.12.18
 * Time: 14:24
 */

namespace Dogovor24\Queue;


use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Foundation\Bus\PendingDispatch;

trait QueuePackageDispatchable
{
    use Dispatchable;

    public static function dispatch()
    {
        $dispatcher = new QueuePackagePendingDispatch();
        foreach (QueuePackage::getQueues() as $queue) {
            $class = new static(...func_get_args());
            $class->onQueue($queue);
            $dispatcher->addJob(new PendingDispatch($class));
        }
        return $dispatcher;
    }
}