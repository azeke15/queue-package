<?php
/**
 * Created by PhpStorm.
 * User: azamat
 * Date: 29.12.18
 * Time: 11:54
 */

namespace Dogovor24\Queue;

use Illuminate\Foundation\Bus\PendingDispatch;


class QueuePackagePendingDispatch extends PendingDispatch
{
    /**
     * The job.
     *
     * @var mixed
     */
    protected $jobs;

    /**
     * Create a new pending job dispatch.
     *
     * @param  mixed  $job
     * @return void
     */
    public function __construct($job = null)
    {
        if ($job)
            $this->jobs[] = $job;
    }

    public function addJob($job)
    {
        $this->jobs[] = $job;
        
        return $this;
    }

    /**
     * Set the desired connection for the job.
     *
     * @param  string|null  $connection
     * @return $this
     */
    public function onConnection($connection)
    {
        foreach ($this->jobs as $job) {
            $job->onConnection($connection);
        }

        return $this;
    }

    /**
     * Set the desired queue for the job.
     *
     * @param  string|null  $queue
     * @return $this
     */
    public function onQueue($queue)
    {
        foreach ($this->jobs as $job) {
            $job->onQueue($queue);
        }
        $this->job->onQueue($queue);

        return $this;
    }

    /**
     * Set the desired connection for the chain.
     *
     * @param  string|null  $connection
     * @return $this
     */
    public function allOnConnection($connection)
    {
        foreach ($this->jobs as $job) {
            $job->allOnConnection($connection);
        }

        return $this;
    }

    /**
     * Set the desired queue for the chain.
     *
     * @param  string|null  $queue
     * @return $this
     */
    public function allOnQueue($queue)
    {
        foreach ($this->jobs as $job) {
            $job->allOnQueue($queue);
        }

        return $this;
    }

    /**
     * Set the desired delay for the job.
     *
     * @param  \DateTime|int|null  $delay
     * @return $this
     */
    public function delay($delay)
    {
        foreach ($this->jobs as $job) {
            $job->delay($delay);
        }

        return $this;
    }

    /**
     * Set the jobs that should run if this job is successful.
     *
     * @param  array  $chain
     * @return $this
     */
    public function chain($chain)
    {
        foreach ($this->jobs as $job) {
            $job->chain($chain);
        }

        return $this;
    }

    /**
     * Handle the object's destruction.
     *
     * @return void
     */
    public function __destruct()
    {
        /*$dispatcher = app(Dispatcher::class);
        foreach ($this->jobs as $job) {
            $dispatcher->dispatch($job);
        }*/
    }
}
