<?php

return [
    'subscription-service'  => 'subscription-service-queue',
    'authorization-service' => 'authorization-service-queue',
    'billing-service'       => 'billing-service-queue',
    'document-service'      => 'document-service-queue',
    'notification-service'  => 'notification-service-queue'
];