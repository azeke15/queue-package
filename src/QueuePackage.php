<?php
/**
 * Created by PhpStorm.
 * User: azamat
 * Date: 27.12.18
 * Time: 9:35
 */

namespace Dogovor24\Queue;

class QueuePackage
{

    public static function getQueues() {
        $config = config('queue-package');
        return $config;
    }

    public function dispatchJob($jobClassName, $data)
    {
        if (method_exists($jobClassName, 'dispatch')) {
            $jobClassName::dispatch($data);
        }
    }

}