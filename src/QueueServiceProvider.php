<?php
/**
 * Created by PhpStorm.
 * User: azamat
 * Date: 26.12.18
 * Time: 16:24
 */

namespace Dogovor24\Queue;


use Illuminate\Support\ServiceProvider;

class QueueServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/queue-package.php', 'queue-package'
        );

        $this->app->singleton('queue-package', function () {
            return new QueuePackage();
        });
    }
}