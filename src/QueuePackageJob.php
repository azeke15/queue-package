<?php
/**
 * Created by PhpStorm.
 * User: azamat
 * Date: 28.12.18
 * Time: 14:33
 */

namespace Dogovor24\Queue;

use App\Events\TestEvent;
use App\Providers\EventServiceProvider;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

abstract class QueuePackageJob implements ShouldQueue
{
    use QueuePackageDispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle()
    {
        if (isset(app()->getProvider(EventServiceProvider::class)->listens()[$this->eventClassName()])) {
            $this->eventProcessing();
        }
    }

    abstract function eventProcessing();
    abstract function eventClassName();
}
